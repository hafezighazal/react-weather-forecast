import React, { Component } from 'react'


const TableHeader = () => {

    return (
        <thead>
            <tr>
                <th>num</th>
                <th>name</th>
                <th>last name</th>
            </tr>

        </thead>

    )

}

const TableBody = props => {
    const rows = props.characterData.map((row, index) => {

        return (
            <tr key = {index}>
                <td>{row.num}</td>
                <td>{row.name}</td>
                <td>{row.lastName}</td>
            </tr>
        )

    })

    return (
        <tbody>{rows}</tbody>
    )
}



class Table extends Component {
    render() {

        const { characterData } = this.props;

        return (

            <table className='table table-dark mt-4'>
                <TableHeader />
                <TableBody characterData={characterData} />
            </table>


        )
    }
}


export default Table;